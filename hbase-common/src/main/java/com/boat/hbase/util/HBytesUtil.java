package com.boat.hbase.util;

import java.math.BigDecimal;

import org.apache.hadoop.hbase.util.Bytes;

public class HBytesUtil extends Bytes {

	public static byte[] toBytesByString(String val) {
		return toBytes(val, "");
	}

	public static byte[] toBytesByLong(Long val) {
		return toBytes(val, 0l);
	}

	public static byte[] toBytesByInteger(Integer val) {
		return toBytes(val, 0);
	}

	public static byte[] toBytesByBoolean(Boolean val) {
		return toBytes(val, false);
	}

	public static byte[] toBytes(Object val, Object defaultVal) {
		if (val == null) {
			val = defaultVal;
		}
		if (!val.getClass().getName().equals(defaultVal.getClass().getName())) {
			throw new RuntimeException();
		} else {
			String defalutClassName = defaultVal.getClass().getName();
			if (defalutClassName.equals(String.class.getName())) {
				return toBytes(val.toString());
			}
			if (defalutClassName.equals(Boolean.class.getName())) {
				return toBytes((boolean) val);
			}
			if (defalutClassName.equals(Long.class.getName())) {
				return toBytes((long) val);
			}
			if (defalutClassName.equals(Float.class.getName())) {
				return toBytes((float) val);
			}
			if (defalutClassName.equals(Double.class.getName())) {
				return toBytes((double) val);
			}
			if (defalutClassName.equals(Integer.class.getName())) {
				return toBytes((int) val);
			}
			if (defalutClassName.equals(Short.class.getName())) {
				return toBytes((short) val);
			}
			if (defalutClassName.equals(BigDecimal.class.getName())) {
				return toBytes((BigDecimal) val);
			}
		}
		return null;
	}

	public static Object toObject(byte[] val, String className) {
		switch (className) {
		case "java.lang.String":
			return Bytes.toString(val);
		case "boolean":
			return Bytes.toBoolean(val);
		case "java.lang.Boolean":
			return Bytes.toBoolean(val);
		case "long":
			return Bytes.toLong(val);
		case "java.lang.Long":
			return Bytes.toLong(val);
		case "float":
			return Bytes.toFloat(val);
		case "java.lang.Float":
			return Bytes.toFloat(val);
		case "double":
			return Bytes.toDouble(val);
		case "java.lang.Double":
			return Bytes.toDouble(val);
		case "int":
			return Bytes.toInt(val);
		case "java.lang.Integer":
			return Bytes.toInt(val);
		case "short":
			return Bytes.toShort(val);
		case "java.lang.Short":
			return Bytes.toShort(val);
		case "java.math.BigDecimal":
			return Bytes.toBigDecimal(val);
		default:
			return null;
		}
	}

	public static void main(String[] args) {
		System.out.println(String.class.getName());
		System.out.println(Boolean.class.getName());
		System.out.println(Long.class.getName());
		System.out.println(Float.class.getName());
		System.out.println(Double.class.getName());
		System.out.println(Integer.class.getName());
		System.out.println(Short.class.getName());
		System.out.println(BigDecimal.class.getName());
		System.out.println(int.class.getName());
	}
}
