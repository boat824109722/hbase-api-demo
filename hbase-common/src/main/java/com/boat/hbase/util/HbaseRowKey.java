package com.boat.hbase.util;

import java.io.UnsupportedEncodingException;

import org.apache.hadoop.hbase.util.MD5Hash;

import com.boat.hbase.common.HbaseConstant;

/**
 * @Description: Hbase rowkey生成
 * @author boat
 * @date 2017年12月28日 下午7:16:56
 * @version V1.0
 */

public class HbaseRowKey {
	/**
	 * Hbase rowkey生成
	 * 
	 * @param id，主键，唯一
	 * @return rowkey
	 */
	public static byte[] getRowkey(String id) {
		try {
			String keyToHash = MD5Hash.getMD5AsHex(id.getBytes(HbaseConstant.charSet)).substring(0, 8);
			String key = keyToHash + id;
			return key.getBytes(HbaseConstant.charSet);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
}
