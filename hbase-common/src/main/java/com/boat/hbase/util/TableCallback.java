package com.boat.hbase.util;

/**   
* @Title: TableCallback.java
* @Description: Hbase回调类
*/

import org.apache.hadoop.hbase.client.Table;

/** 
 * @Description: Hbase回调类
 * @author boat 
 * @date 2017年12月27日 上午9:30:03 
 * @version V1.0
 */
public interface TableCallback<T> {
	
	/**
	 * @Description Hbase回调函数
	 * @param table
	 * @return
	 * @throws Throwable
	 */
	T doInTable(Table table) throws Throwable;

}
