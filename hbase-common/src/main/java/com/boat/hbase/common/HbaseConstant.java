package com.boat.hbase.common;

/**
 * @Description: Hbase 表名定义
 * @author boat
 * @date 2017年12月28日 上午10:29:10
 * @version V1.0
 */

public class HbaseConstant {
	
	/**
	 * 编码
	 */
	public static final String charSet = "UTF-8";

	/**
	 * 所有表预设只有一个列族
	 */
	public static final byte[] columnFamily = "cf".getBytes();
	
	/**
	 * 学生表
	 */
	public static final String STUDENT = "STUDENT";

}
