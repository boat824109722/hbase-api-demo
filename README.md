# hbase-api-demo
hbase api 实例，项目对hbase api进行封装，创建表（预分区），删除表，增删改查等（connection的创建）

### hbase的预分区和rowkey的散列知识可以参考下面链接：
#### [hbase系列-Hbase热点问题、数据倾斜和rowkey的散列设计](http://blog.csdn.net/weixin_41279060/article/details/78855679)

### 项目说明：  
1、hbase-table-init 为初始化Hbase表结构模块，主要是利用哈希对Region进行预分区和划分splitkey的范围；   
2、hbase-api-client 为客户端模块，主要是插入和查询 ，值得注意的是，hbase的connection获取是很慢的，这里采用了单例模式；   
3、hbase-common 为存储一些工具类和共同变量的公共模块；    
  

### 执行结果（集群共有3个节点，master60，node61，node62）：
#### 1、创建10个预分区
```
public class StudentInit {
	
	public static void main(String[] args) {
		try {
			//如果表存在，则删除
			HbaseTableInit.deleteTable(HbaseConstant.STUDENT);
			
			//预分区，划分10个Region
			HashChoreWoker worker = new HashChoreWoker(1000000, 10);
			byte[][] splitKeys = worker.calcSplitKeys();
			//创建表
			HbaseTableInit.createTable(HbaseConstant.STUDENT, splitKeys, HbaseConstant.columnFamily);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
```

创建分区后，10个分区均衡分布在集群的3个节点上(3\3\4)，如下：
![输入图片说明](https://gitee.com/uploads/images/2018/0115/112342_d1e2d70f_1705914.png "1.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0115/112638_d4851c21_1705914.png "2.png")

#### 2、向表中插入10000条数据
```
@Test
public void testSaveBatch() throws Exception {
	List<Student> students = new ArrayList<Student>();
	for (int i = 0; i < 10000; i++) {
		Student student = new Student();
		student.setStuNO("2011410" + i);
		student.setName("学生" + i);
		student.setPhoneNum("1810000" + i);
		student.setAge(18);
		student.setIdCard("4417011992011" + i);
		students.add(student);
	}
	try {
		studentService.saveBatch(students);
	} catch (Exception e) {
		e.printStackTrace();
	}
}
```

插入数据后，各个节点的请求数如下（写请求均衡分布在10个region上，也就是均衡分布在3个节点上）：
![输入图片说明](https://gitee.com/uploads/images/2018/0115/113050_5190e063_1705914.png "3.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0115/113115_b73ab9f1_1705914.png "4.png")
