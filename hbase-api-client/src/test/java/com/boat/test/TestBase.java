package com.boat.test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.boat.Application;

/**
 * @Description: 测试基类
 * @author boat
 * @date 2017年12月29日 上午10:23:07
 * @version V1.0
 */

@RunWith(SpringJUnit4ClassRunner.class) // SpringJUnit支持，由此引入Spring-Test框架支持！
@SpringBootTest(classes = Application.class) // 指定spring-boot的启动类
public class TestBase {

}
