package com.boat.test.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.mortbay.util.ajax.JSON;

import com.boat.bean.Student;
import com.boat.hbase.util.HbaseConectionManager;
import com.boat.service.StudentService;
import com.boat.test.TestBase;

/**
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2017年12月29日 上午10:24:48
 * @version V1.0
 */

public class TestStudentService extends TestBase {

	@Resource
	private HbaseConectionManager hbaseConectionManager;

	@Resource
	private StudentService studentService;

	@Test
	public void testGetConnect() {
		try {
			for (int i = 0; i < 10; i++) {
				long startTime = System.currentTimeMillis();// 记录开始时间
				hbaseConectionManager.getConnection();
				long endTime = System.currentTimeMillis();// 记录结束时间
				float excTime = (float) (endTime - startTime) / 1000;
				System.out.println("第" + i + "次执行时间：" + excTime + "s");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSave() {
		Student student = new Student();
		student.setStuNO("2011410S001");
		student.setName("学生S01");
		student.setPhoneNum("18000000001");
		student.setAge(18);
		student.setIdCard("44170119920121234");
		studentService.save(student);
	}

	@Test
	public void testSaveBatch() throws Exception {
		hbaseConectionManager.getConnection();
		List<Student> students = new ArrayList<Student>();
		for (int i = 0; i < 10000; i++) {
			Student student = new Student();
			student.setStuNO("2011410" + i);
			student.setName("学生" + i);
			student.setPhoneNum("1810000" + i);
			student.setAge(18);
			student.setIdCard("4417011992011" + i);
			students.add(student);
		}
		try {
			long startTime = System.currentTimeMillis();// 记录开始时间
			studentService.saveBatch(students);
			long endTime = System.currentTimeMillis();// 记录结束时间

			float excTime = (float) (endTime - startTime) / 1000;

			System.out.println("执行时间：" + excTime + "s");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testFind() {
		Student student = studentService.findByStuNo("20114101");
		System.out.println(student.toString());
	}
}
