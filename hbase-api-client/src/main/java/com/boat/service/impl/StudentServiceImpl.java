package com.boat.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Scan;
import org.springframework.stereotype.Service;

import com.boat.bean.Student;
import com.boat.hbase.common.HbaseConstant;
import com.boat.hbase.util.BeanToPut;
import com.boat.hbase.util.HbaseRowKey;
import com.boat.hbase.util.HbaseTemplate;
import com.boat.service.StudentService;

/**
 * @Description: Student 操作
 * @author boat
 * @date 2017年12月28日 下午6:35:47
 * @version V1.0
 */

@Service("studentService")
public class StudentServiceImpl implements StudentService {

	@Resource
	private HbaseTemplate hbaseTemplate;

	public void save(Student student) {
		Put studentPut = BeanToPut.beanToPut(student, HbaseRowKey.getRowkey(student.getStuNO()), HbaseConstant.columnFamily);
		hbaseTemplate.save(HbaseConstant.STUDENT, studentPut);
	}

	public void saveBatch(List<Student> students) {
		List<Put> studentPuts = new ArrayList<Put>();
		for (Student student : students) {
			Put studentPut = BeanToPut.beanToPut(student, HbaseRowKey.getRowkey(student.getStuNO()), HbaseConstant.columnFamily);
			studentPuts.add(studentPut);
		}
		hbaseTemplate.saveBatch(HbaseConstant.STUDENT, studentPuts);
	}

	public Student findByStuNo(String stuNO) {
		Scan scan = new Scan();
		byte[] rowkey = HbaseRowKey.getRowkey(stuNO);
		scan.setStartRow(rowkey);
		scan.setStopRow(rowkey);
		List<Object> objectList = hbaseTemplate.findObjects(HbaseConstant.STUDENT, scan, Student.class);
		if (objectList != null && objectList.size() > 0) {
			return (Student) objectList.get(0);
		} else {
			return null;
		}
	}

}
