package com.boat.service;

import java.util.List;

import com.boat.bean.Student;

/**
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2017年12月28日 下午6:35:16
 * @version V1.0
 */

public interface StudentService {
	public void save(Student student);

	public void saveBatch(List<Student> students);

	public Student findByStuNo(String stuNO);
}
