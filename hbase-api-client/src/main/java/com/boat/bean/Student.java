package com.boat.bean;

/**
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2017年12月28日 下午4:29:21
 * @version V1.0
 */

public class Student {

	private String stuNO;

	private String name;

	private int age;

	private String idCard;

	private String address;

	private String phoneNum;

	public String getStuNO() {
		return stuNO;
	}

	public void setStuNO(String stuNO) {
		this.stuNO = stuNO;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String toString() {
		return "stuNo=" + stuNO + " name=" + name + " age=" + age + " idCard=" + idCard + " address=" + address + " phoneNum=" + phoneNum;
	}
}
