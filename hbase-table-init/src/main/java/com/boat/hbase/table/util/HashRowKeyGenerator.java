package com.boat.hbase.table.util;

import java.io.UnsupportedEncodingException;
import java.util.Random;

import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.MD5Hash;

import com.boat.hbase.common.HbaseConstant;

/**
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2017年12月28日 上午11:00:13
 * @version V1.0
 */

public class HashRowKeyGenerator {
	private long currentId = 1;
	private long currentTime = System.currentTimeMillis();
	private Random random = new Random();

	public byte[] nextId() throws UnsupportedEncodingException {
		try {
			currentTime += random.nextInt(1000);
			byte[] lowT = Bytes.copy(Long.toString(currentTime).getBytes(HbaseConstant.charSet), 4, 4);
			byte[] lowU = Bytes.copy(Long.toString(currentId).getBytes(HbaseConstant.charSet));
			return MD5Hash.getMD5AsHex(Bytes.add(lowU, lowT)).substring(0, 8).getBytes(HbaseConstant.charSet);
		} finally {
			currentId++;
		}
	}

}
