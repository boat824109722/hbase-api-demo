package com.boat.hbase.table.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.io.compress.Compression.Algorithm;
import org.apache.hadoop.hbase.regionserver.BloomType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 建表工具类
 * 
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2017年12月28日 上午9:17:15
 * @version V1.0
 */
public class HbaseTableInit {

	static Configuration conf;

	private static final Logger logger = LoggerFactory.getLogger(HbaseTableInit.class);

	static {
		conf = HBaseConfiguration.create();
	}

	/**
	 * 删除一个表
	 * 
	 * @param tableName
	 *            删除的表名
	 */
	public static void deleteTable(String tableName) throws Exception {
		Connection conn = null;
		HBaseAdmin hBaseAdmin = null;
		try {
			conn = ConnectionFactory.createConnection(conf);
			hBaseAdmin = (HBaseAdmin) conn.getAdmin();
			if (!hBaseAdmin.tableExists(tableName)) {
				logger.info("删除的表不存在！");
				return;
			}
			hBaseAdmin.disableTable(tableName);// 禁用表
			hBaseAdmin.deleteTable(tableName);// 删除表
			logger.info("删除表成功!");
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			// 关闭释放资源
			if (hBaseAdmin != null) {
				hBaseAdmin.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}

	public static void createTable(String tableName, byte[][] splits, byte[] family) throws Exception {
		Connection conn = null;
		HBaseAdmin hBaseAdmin = null;
		try {
			conn = ConnectionFactory.createConnection(conf);
			hBaseAdmin = (HBaseAdmin) conn.getAdmin();
			if (hBaseAdmin.tableExists(tableName)) {
				logger.error(tableName + "表已存在！");
			} else {
				HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf(tableName));
				HColumnDescriptor hColumnDescriptor = new HColumnDescriptor(family);
				/**
				 * setTimeToLive:指定最大的TTL,单位是ms,过期数据会被自动删除。默认是2147483647，int的最大值，代表forever（永久的）。视业务而定。
				 * setInMemory:指定是否放在内存中，对小表有用，可用于提高效率。默认关闭。一般不设置，使用默认值，适合比较小的列族，例如登录用的用户表。
				 * setBloomFilter:指定是否使用BloomFilter,可提高随机查询效率。默认关闭。一般设置成BloomType.ROW
				 * setCompressionType:设定数据压缩类型。默认无压缩。一般设置Algorithm.SNAPPY,Hbase要开启SNAPPY压缩
				 * setMaxVersions:指定数据最大保存的版本个数。默认为3。一般设置成1
				 */
				hColumnDescriptor.setMaxVersions(1);
				hColumnDescriptor.setBloomFilterType(BloomType.ROW);
				hColumnDescriptor.setCompressionType(Algorithm.SNAPPY);
				hTableDescriptor.addFamily(hColumnDescriptor);
				hBaseAdmin.createTable(hTableDescriptor, splits);
				logger.info(tableName + "： 建表成功!");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			// 关闭释放资源
			if (hBaseAdmin != null) {
				hBaseAdmin.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}

}
