package com.boat.hbase.table.init;

import com.boat.hbase.common.HbaseConstant;
import com.boat.hbase.table.util.HashChoreWoker;
import com.boat.hbase.table.util.HbaseTableInit;

/**
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2017年12月28日 上午10:22:23
 * @version V1.0
 */

public class StudentInit {
	
	public static void main(String[] args) {
		try {
			//如果表存在，则删除
			HbaseTableInit.deleteTable(HbaseConstant.STUDENT);
			
			//预分区，划分10个Region
			HashChoreWoker worker = new HashChoreWoker(1000000, 10);
			byte[][] splitKeys = worker.calcSplitKeys();
			//创建表
			HbaseTableInit.createTable(HbaseConstant.STUDENT, splitKeys, HbaseConstant.columnFamily);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
